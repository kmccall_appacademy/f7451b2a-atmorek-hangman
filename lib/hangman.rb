class Hangman
  attr_reader :guesser, :referee, :board, :guesses
  attr_accessor :wrong_guesses

  HANGMAN_HASH = {
    0 => ["
     +---+
     |   |
         |
         |
         |
         |
  ========="],
    1 => ["
    +---+
    |   |
    O   |
        |
        |
        |
  ========="],
    2 => ["
    +---+
    |   |
    O   |
    |   |
        |
        |
  ========="],
    3 => ["
    +---+
    |   |
    O   |
   /|   |
        |
        |
  ========="],
    4 => ["
    +---+
    |   |
    O   |
   /|\\  |
        |
        |
  ========="],
    5 => ["
    +---+
    |   |
    O   |
   /|\\  |
   /    |
        |
  ========="],
    6 => ["
    +---+
    |   |
    O   |
   /|\\  |
   / \\  |
        |
  ========="]
  }

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @guesses = []
    @wrong_guesses = 0
  end

  def setup
    length = referee.pick_secret_word
    guesser.register_secret_length(length)
    @board = Array.new(length)
    display_hangman
  end

  def take_turn
    display_board
    letter = guesser.guess(board)
    guesses << letter.upcase
    puts

    indices = referee.check_guess(letter, board)
    @wrong_guesses += 1 if indices == []
    update_board(indices, letter)

    system "clear"
    display_hangman
    guesser.handle_response(letter, indices)
    puts "Previous guesses: #{guesses}", ""
  end

  def display_board
    print "Secret word: "
    board.each { |el| print el.nil? ? "_" : el }
    puts
  end

  def update_board(indices, letter)
    indices.each { |i| board[i] = letter }
  end

  def display_hangman
    puts HANGMAN_HASH[wrong_guesses]
  end

  def won?
    board.none?(&:nil?)
  end

  def game_over?
    wrong_guesses == 6
  end

  def conclude
    display_board

    if won?
      puts "Congratulations, you win!!"
    else
      puts "You're out of guesses! You lose!!"
    end

    if referee.is_a?(ComputerPlayer)
      puts "The word was '#{referee.secret_word}'."
    end
  end

  def play
    setup
    take_turn until won? || game_over?
    conclude
  end
end

class HumanPlayer
  attr_reader :secret_word, :secret_length, :potential_guesses

  def initialize
    @potential_guesses = ("a".."z").to_a
  end

  def pick_secret_word
    print "How long is secret word?: "
    @secret_length = gets.chomp.to_i
  end

  def guess(board)
    print "Guess a letter: "

    letter = gets.chomp.downcase

    until valid_guess?(letter)
      letter = get_valid_guess(letter)
    end

    potential_guesses.delete(letter)
    letter
  end

  def check_guess(letter, board)
    puts "Is '#{letter}' in your secret word?"
    puts "If it is not in your word, type '0'"
    print "If it is, type the spaces it occurs."
    puts "(i.e. type '1, 3' if it appears in the first and third spaces)\n"
    print "Where does '#{letter}' occur?: "

    input = gets.chomp
    return [] if input == "0"

    # Subtract one from each number to convert user input to arr index
    indices = input.split(", ").map { |i| i.to_i - 1 }

    until valid_check?(indices, board)
      indices = get_valid_check(letter)
    end

    indices
  end

  def register_secret_length(length)
    @secret_length = length
    puts "The secret word is #{length} letters long."
  end

  def handle_response(letter, indices)
    if indices == []
      puts "Sorry! #{letter.upcase} is not in the word!"
    else
      puts "Correct! #{letter.upcase} is in the word!"
    end
    puts
  end

  def valid_guess?(letter)
    potential_guesses.include?(letter)
  end

  def get_valid_guess(letter)
    if ("a".."z").to_a.include?(letter)
      puts "You've already guessed that letter."
    else
      puts "Your guess must be a single letter."
    end

    print "Guess a letter: "
    gets.chomp.downcase
  end

  def valid_check?(indices, board)
    return false unless indices.all? { |i| (0...secret_length).include?(i) }
    indices.all? { |i| board[i].nil? }
  end

  def get_valid_check(letter)
    puts "The positions you enter must be valid empty spaces."
    print "Where does '#{letter}' occur?: "
    gets.chomp.split(", ").map { |i| i.to_i - 1 }
  end

end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :secret_length,
              :potential_guesses, :candidate_words

  def initialize(dictionary = File.readlines("lib/dictionary.txt").map(&:chomp))
    @dictionary = dictionary
    @potential_guesses = ("a".."z").to_a
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    secret_word.length
  end

  def guess(board)
    print "Guess a letter: "
    sleep(1)

    letter = most_common_letter
    potential_guesses.delete(letter)
    puts letter

    sleep(1)
    letter
  end

  def check_guess(letter, _)
    secret_word.chars.each_index.select do |i|
      secret_word[i] == letter
    end
  end

  def register_secret_length(length)
    @secret_length = length
    puts "The secret word is #{length} letters long."
    @candidate_words = dictionary.select! { |word| word.length == length }
  end

  def handle_response(letter, indices)

    if indices == []
      puts "Sorry! #{letter.upcase} is not in the word!"
    else
      puts "Correct! #{letter.upcase} is in the word!"
    end
    puts

    candidate_words.select! do |word|
      word.count(letter) == indices.length &&
      indices.all? { |i| word[i] == letter }
    end
  end

  def most_common_letter
    letters = candidate_words.join
    counts = {}

    potential_guesses.each { |ch| counts[ch] = letters.count(ch) }

    counts.sort_by { |_, v| v }[-1][0]
  end

end

if __FILE__ == $PROGRAM_NAME
  print "Guesser: Computer (y/n)? "
  if gets.chomp.downcase == "y"
    guesser = ComputerPlayer.new
  else
    guesser = HumanPlayer.new
  end

  print "Referee: Computer (y/n)? "
  if gets.chomp == "y"
    referee = ComputerPlayer.new
  else
    referee = HumanPlayer.new
  end

  Hangman.new(guesser: guesser, referee: referee).play

end
